package com.kriswick.todoapi.repository;

import com.kriswick.todoapi.model.Todo;
import com.kriswick.todoapi.model.TodoStatus;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.kriswick.todoapi.fixture.TodoFixture.todo;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.*;

public class TodoRepositoryInMemoryImplTest {

    private TodoRepository todoRepository;

    @Before
    public void setup(){
        todoRepository = new TodoRepositoryInMemoryImpl();
    }

    @Test
    public void saveAddsTodoToRepositoryAndReturnsTheSame(){
        Todo todo = todo();

        Todo result = todoRepository.save(todo);

        assertThat(result, equalTo(todo));
        assertThat(todoRepository.findAll(), hasSize(1));
        assertThat(todoRepository.findAll().get(0), equalTo(todo));
    }

    @Test
    public void findOneReturnsEmptyIfNotFound(){
        assertThat(todoRepository.findOne(UUID.randomUUID()), equalTo(Optional.empty()));
    }

    @Test
    public void findOneReturnsTodoWhenFound(){
        Todo todo = todo();
        todoRepository.save(todo);

        assertThat(todoRepository.findOne(todo.getId()), equalTo(Optional.of(todo)));
    }

    @Test
    public void findAllReturnsEmptyListWhenNothingIsInStore(){
        assertThat(todoRepository.findAll(), equalTo(new ArrayList<Todo>()));
    }

    @Test
    public void findAllReturnsTodosInStoreInTheOrderTheyWereAdded(){
        Todo todo1 = todo();
        todoRepository.save(todo1);
        Todo todo2 = todo();
        todoRepository.save(todo2);
        assertThat(todoRepository.findAll(), equalTo(Stream.of(todo1, todo2).collect(Collectors.toList())));
    }

    @Test
    public void findAllByStatusReturnsOnlyTodosWithGivenStatus(){
        Todo todo1 = todo(TodoStatus.PENDING);
        todoRepository.save(todo1);

        Todo todo2 = todo(TodoStatus.DONE);
        todoRepository.save(todo2);

        Todo todo3 = todo(TodoStatus.PENDING);
        todoRepository.save(todo3);

        List<Todo> results = todoRepository.findAllByStatus(TodoStatus.PENDING);

        assertThat(results, equalTo(new ArrayList<Todo>(Stream.of(todo1, todo3).collect(Collectors.toList()))));
    }

    @Test
    public void saveReplacesExistingTodo(){
        Todo todo = todo();
        todo.setName("Name1");

        todoRepository.save(todo);
        assertThat(todoRepository.findAll(), hasSize(1));

        todo.setName("Name2");
        todoRepository.save(todo);
        assertThat(todoRepository.findAll(), hasSize(1));
        assertThat(todoRepository.findAll().get(0).getName(), equalTo("Name2"));
    }

}