package com.kriswick.todoapi.controller;

import com.kriswick.todoapi.model.exception.ApiError;
import com.kriswick.todoapi.model.exception.TodoNotFoundException;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.WebRequest;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;

public class ApiExceptionHandlerTest {

    @Test
    public void todoNotFoundExceptionReturnsApiError(){

        TodoNotFoundException exception = new TodoNotFoundException(UUID.randomUUID());
        ResponseEntity<Object> objectResponseEntity = new ApiExceptionHandler().handleException(exception,
                Mockito.mock(WebRequest.class));

        assertThat(objectResponseEntity, notNullValue());
        assertThat(objectResponseEntity.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
        assertThat(objectResponseEntity.getBody(), instanceOf(ApiError.class));
        ApiError apiError = (ApiError) objectResponseEntity.getBody();
        assertThat(apiError.getErrorCode(), equalTo(exception.getErrorCode()));
        assertThat(apiError.getErrorMessage(), equalTo(exception.getErrorMessage()));
    }
}