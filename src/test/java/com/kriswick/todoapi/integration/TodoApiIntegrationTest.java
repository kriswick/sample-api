package com.kriswick.todoapi.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.kriswick.todoapi.TodoApiApplication;
import com.kriswick.todoapi.model.Todo;
import com.kriswick.todoapi.model.TodoCreate;
import com.kriswick.todoapi.model.TodoStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.kriswick.todoapi.fixture.TodoFixture.todoCreate;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT, classes = {TodoApiApplication.class})
@AutoConfigureMockMvc
public class TodoApiIntegrationTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setup() {

    }

    @Test
    public void createTodoReturnsCreatedTodo() throws Exception {
        TodoCreate request = todoCreate();
        mockMvc.perform(post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getObjectMapper().writeValueAsString(request)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(request.getName()))
                .andExpect(jsonPath("$.description").value(request.getDescription()))
                .andExpect(jsonPath("$.dueDate").value(request.getDueDate().format(DateTimeFormatter.ISO_DATE)))
                .andExpect(jsonPath("$.status").value(TodoStatus.PENDING.name()));
    }

    @Test
    public void getTodoReturns404WhenNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/todos/" + UUID.randomUUID()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getTodoReturnsTodoWhenFound() throws Exception {
        Todo todo = createTodo(todoCreate());

        mockMvc.perform(MockMvcRequestBuilders.get("/todos/" + todo.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(todo.getId().toString()));
    }

    //TODO test the other api operations

    private Todo createTodo(TodoCreate request) throws Exception {
        final List<Todo> todos = new ArrayList<Todo>();
        mockMvc.perform(post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getObjectMapper().writeValueAsString(request)))
                .andExpect(status().isCreated())
                .andDo(mockMvcResult -> {
                    String json = mockMvcResult.getResponse().getContentAsString();
                    todos.add(getObjectMapper().readValue(json, Todo.class));
                });

        return todos.get(0);
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return mapper;
    }

}
