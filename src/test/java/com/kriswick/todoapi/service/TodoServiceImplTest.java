package com.kriswick.todoapi.service;

import com.kriswick.todoapi.model.Todo;
import com.kriswick.todoapi.model.TodoCreate;
import com.kriswick.todoapi.model.TodoFilter;
import com.kriswick.todoapi.model.TodoStatus;
import com.kriswick.todoapi.model.exception.CannotUpdateIdInTodoException;
import com.kriswick.todoapi.model.exception.TodoNotFoundException;
import com.kriswick.todoapi.model.exception.TodoUnacceptableException;
import com.kriswick.todoapi.repository.TodoRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.kriswick.todoapi.fixture.TodoFixture.todo;
import static com.kriswick.todoapi.fixture.TodoFixture.todoCreate;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TodoServiceImplTest {

    @Mock
    private TodoRepository todoRepository;

    @InjectMocks
    private TodoServiceImpl todoService;

    @Test
    public void listReturnsEverythingFromRepositoryWhenFiltersAreEmpty(){
        TodoFilter filter = new TodoFilter();

        List<Todo> todoList = Stream.of(todo(), todo()).collect(Collectors.toList());
        when(todoRepository.findAll()).thenReturn(todoList);

        List<Todo> results = todoService.list(filter);

        assertThat(results, equalTo(todoList));
    }

    @Test
    public void listReturnsTodosWithGivenStatusFromRepositoryWhenFilteredByStatus(){
        TodoFilter filter = new TodoFilter();
        filter.setStatus(Optional.of(TodoStatus.PENDING));

        List<Todo> todoList = Stream.of(todo(), todo()).collect(Collectors.toList());
        when(todoRepository.findAllByStatus(TodoStatus.PENDING)).thenReturn(todoList);

        List<Todo> results = todoService.list(filter);

        assertThat(results, equalTo(todoList));
    }

    @Test(expected = TodoUnacceptableException.class)
    public void createShouldRejectUnacceptableTodos(){
        TodoCreate todoCreate = todoCreate();
        todoCreate.setName("Plan murder the neighbour.");
        todoCreate.setDescription("Investigate best way to murder the annoying neighbour");

        todoService.create(todoCreate);
    }

    @Test
    public void createValidTodoStoresNewTodoInRepositoryAndReturnsTheSame(){
        TodoCreate todoCreate = todoCreate();

        when(todoRepository.save(any())).then(AdditionalAnswers.returnsFirstArg());

        Todo todo = todoService.create(todoCreate);

        verify(todoRepository).save(any(Todo.class));

        assertThat(todo, notNullValue());
        assertThat(todo.getId(), notNullValue());
        assertThat(todo.getName(), equalTo(todoCreate.getName()));
        assertThat(todo.getDescription(), equalTo(todoCreate.getDescription()));
        assertThat(todo.getDueDate(), equalTo(todoCreate.getDueDate()));
        assertThat(todo.getStatus(), equalTo(TodoStatus.PENDING));

    }

    @Test(expected = TodoNotFoundException.class)
    public void getTodoThrowsNotFoundExceptionWhenGivenIdDoesNotExist(){
        UUID id = UUID.randomUUID();
        when(todoRepository.findOne(id)).thenReturn(Optional.empty());

        todoService.get(id);
    }

    @Test
    public void getTodoReturnsTheTodoByIdWhenFound(){
        Todo todo = todo();
        UUID id = todo.getId();
        when(todoRepository.findOne(id)).thenReturn(Optional.of(todo));

        Todo result = todoService.get(id);

        assertThat(result, equalTo(todo));
    }

    @Test(expected = TodoNotFoundException.class)
    public void replaceThrowsNotFoundExceptionWhenGivenIdDoesNotExist(){
        UUID id = UUID.randomUUID();
        when(todoRepository.findOne(id)).thenReturn(Optional.empty());

        todoService.replace(id, todo());
    }

    @Test(expected = CannotUpdateIdInTodoException.class)
    public void replaceThrowsExceptionIfAttemptedToUpdateId(){
        UUID id = UUID.randomUUID();
        Todo todo = todo();
        todo.setId(id);
        when(todoRepository.findOne(id)).thenReturn(Optional.of(todo));

        todoService.replace(id, todo());
    }

    @Test
    public void replaceUpdatesExistingTodo(){
        UUID id = UUID.randomUUID();
        Todo todo = todo();
        todo.setId(id);

        when(todoRepository.findOne(id)).thenReturn(Optional.of(todo));
        when(todoRepository.save(any())).then(AdditionalAnswers.returnsFirstArg());

        Todo update = todo();
        update.setId(todo.getId());
        update.setName("new name");
        update.setDescription("new description");
        update.setStatus(TodoStatus.DONE);

        Todo updatedTodo = todoService.replace(todo.getId(), update);

        assertThat(updatedTodo, equalTo(update));
    }
}