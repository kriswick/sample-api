package com.kriswick.todoapi.fixture;

import com.kriswick.todoapi.model.Todo;
import com.kriswick.todoapi.model.TodoCreate;
import com.kriswick.todoapi.model.TodoStatus;

import java.time.LocalDate;
import java.util.Random;
import java.util.UUID;

public class TodoFixture {

    //This is good enough for now. But ideally should use a test data generator library

    public static Todo todo(){
        return todo(TodoStatus.PENDING);
    }

    public static Todo todo(TodoStatus status){
        Todo todo = new Todo();
        todo.setId(UUID.randomUUID());
        todo.setName("Todo-" + new Random().nextInt());
        todo.setDescription("It's just a test todo");
        todo.setDueDate(LocalDate.now().plusDays(3));
        todo.setStatus(status);

        return todo;
    }

    public static TodoCreate todoCreate(){
        TodoCreate todo = new TodoCreate();
        todo.setName("Todo-" + new Random().nextInt());
        todo.setDescription("It's just a test todo");
        todo.setDueDate(LocalDate.now().plusDays(3));

        return todo;
    }
}
