package com.kriswick.todoapi.model;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * Operations on a Todo resource
 */
public interface TodoResource {

    static final String PATH = "/todos";

    @RequestMapping(
            path = PATH,
            method = POST,
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE
    )
    @ResponseStatus(CREATED)
    Todo create(TodoCreate todo);

    @RequestMapping(
            path = PATH + "/{id}",
            method = GET,
            produces = APPLICATION_JSON_VALUE
    )
    Todo get(@PathVariable("id") UUID id);

    @RequestMapping(
            path = PATH + "/{id}",
            method = PUT,
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE
    )
    Todo replace(@PathVariable("id") UUID id, @RequestBody Todo todo);

    @RequestMapping(
            path = PATH,
            method = GET,
            produces = APPLICATION_JSON_VALUE
    )
    List<Todo> list(TodoFilter filter);

}
