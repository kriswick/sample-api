package com.kriswick.todoapi.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;

public class TodoCreate {

    @NotNull
    @Size(min=1, max = 100)
    private String name;

    @Size(min=1, max = 255)
    private String description;

    private LocalDate dueDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TodoCreate todo = (TodoCreate) o;
        return Objects.equals(name, todo.name) &&
                Objects.equals(description, todo.description) &&
                Objects.equals(dueDate, todo.dueDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, dueDate);
    }
}
