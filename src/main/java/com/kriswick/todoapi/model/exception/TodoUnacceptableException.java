package com.kriswick.todoapi.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class TodoUnacceptableException extends ApiException {

    public TodoUnacceptableException(String errorMessage) {
        super("todo.unacceptable", errorMessage);
    }
}
