package com.kriswick.todoapi.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class TodoNotFoundException extends ApiException {

    public TodoNotFoundException(UUID id){
        super("todo.not.found", String.format("TODO with id %s not found", id));
    }
}
