package com.kriswick.todoapi.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class CannotUpdateIdInTodoException extends ApiException {

    public CannotUpdateIdInTodoException() {
        super("todo.cannot.change.id", "Cannot change id of a todo");
    }
}
