package com.kriswick.todoapi.model;

public enum TodoStatus {
    PENDING,
    DONE
}
