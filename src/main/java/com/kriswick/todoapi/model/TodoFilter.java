package com.kriswick.todoapi.model;

import java.util.Optional;

public class TodoFilter {

    private Optional<TodoStatus> status = Optional.empty();

    public Optional<TodoStatus> getStatus() {
        return status;
    }

    public void setStatus(Optional<TodoStatus> status) {
        this.status = status;
    }

}
