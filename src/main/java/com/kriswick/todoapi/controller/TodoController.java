package com.kriswick.todoapi.controller;


import com.kriswick.todoapi.model.Todo;
import com.kriswick.todoapi.model.TodoCreate;
import com.kriswick.todoapi.model.TodoFilter;
import com.kriswick.todoapi.model.TodoResource;
import com.kriswick.todoapi.service.TodoService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController(TodoResource.PATH)
public class TodoController implements TodoResource {

    private final TodoService todoService;

    public TodoController(TodoService todoService){
        this.todoService = todoService;
    }

    @Override
    public Todo create(@Valid @RequestBody TodoCreate todo) {
        return todoService.create(todo);
    }

    @Override
    public Todo get(@PathVariable UUID id) {
        return todoService.get(id);
    }

    @Override
    public Todo replace(@Valid @PathVariable UUID id, @RequestBody Todo todo) {
        return todoService.replace(id, todo);
    }

    @Override
    public List<Todo> list(TodoFilter filter) {
        return todoService.list(filter);
    }

}
