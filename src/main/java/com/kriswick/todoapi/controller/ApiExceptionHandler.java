package com.kriswick.todoapi.controller;

import com.kriswick.todoapi.model.exception.ApiError;
import com.kriswick.todoapi.model.exception.ApiException;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {ApiException.class})
    protected ResponseEntity<Object> handleException(ApiException exception, WebRequest webRequest) {
        ApiError error = new ApiError(exception.getErrorCode(), exception.getErrorMessage());
        HttpStatus status = resolveResponseStatus(exception);

        return handleExceptionInternal(exception, error, new HttpHeaders(), status, webRequest);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ApiError error = new ApiError("invalid.request", ex.getMessage() /*TODO return custom error message */);
        return handleExceptionInternal(ex, error, headers, status, request);
    }

    private HttpStatus resolveResponseStatus(ApiException exception) {
        ResponseStatus annotatedStatus = AnnotatedElementUtils.findMergedAnnotation(exception.getClass(),
                ResponseStatus.class);
        if (annotatedStatus != null) {
            return annotatedStatus.value();
        } else {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }
}
