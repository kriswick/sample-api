package com.kriswick.todoapi.service.mapper;

import com.kriswick.todoapi.model.Todo;
import com.kriswick.todoapi.model.TodoCreate;
import com.kriswick.todoapi.model.TodoStatus;

import java.util.UUID;

public class TodoMapper {

    //This is good enough for now. When requirements/codebase grow might have to employ a mapping library.

    public static Todo toTodo(TodoCreate todoCreate){
        Todo todo = new Todo();
        todo.setId(UUID.randomUUID()); //Assign new Id
        todo.setName(todoCreate.getName());
        todo.setDescription(todoCreate.getDescription());
        todo.setDueDate(todoCreate.getDueDate());
        todo.setStatus(TodoStatus.PENDING); //Always PENDING upon create

        return todo;
    }

    public static Todo update(Todo existingTodo, Todo update){
        existingTodo.setName(update.getName());
        existingTodo.setDescription(update.getDescription());
        existingTodo.setDueDate(update.getDueDate());
        existingTodo.setStatus(update.getStatus());

        return existingTodo;
    }
}
