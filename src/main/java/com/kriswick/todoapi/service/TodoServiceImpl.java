package com.kriswick.todoapi.service;

import com.kriswick.todoapi.model.Todo;
import com.kriswick.todoapi.model.TodoCreate;
import com.kriswick.todoapi.model.TodoFilter;
import com.kriswick.todoapi.model.exception.CannotUpdateIdInTodoException;
import com.kriswick.todoapi.model.exception.TodoNotFoundException;
import com.kriswick.todoapi.model.exception.TodoUnacceptableException;
import com.kriswick.todoapi.repository.TodoRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.kriswick.todoapi.service.mapper.TodoMapper.toTodo;
import static com.kriswick.todoapi.service.mapper.TodoMapper.update;

@Service
public class TodoServiceImpl implements TodoService {

    private final TodoRepository todoRepository;

    public TodoServiceImpl(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    @Override
    public List<Todo> list(TodoFilter filter) {
        Assert.notNull(filter, "filter should not be null");

        //For now only possible filter is status. In future when there are multiple possible filters
        // following code should get refactored into a composable criteria type query
        if (!filter.getStatus().isPresent()){
            return todoRepository.findAll();
        } else {
            return todoRepository.findAllByStatus(filter.getStatus().get());
        }
    }

    @Override
    public Todo create(TodoCreate todoCreate) {
        Assert.notNull(todoCreate, "todoCreate cannot be null when creating");
        validateBusinessRulesForCreate(todoCreate);

        Todo newTodo = toTodo(todoCreate);

        return todoRepository.save(newTodo);
    }

    @Override
    public Todo replace(UUID id, Todo update) {
        Todo existingTodo = get(id);
        validateBusinessRulesForUpdate(existingTodo, update);

        update(existingTodo, update);

        return todoRepository.save(existingTodo);
    }

    @Override
    public Todo get(UUID id) {
        return todoRepository.findOne(id).orElseThrow(() -> new TodoNotFoundException(id));
    }

    private void validateBusinessRulesForCreate(TodoCreate todo) {
        //Not in the requirements. But added to demonstrate business rule validation
        List<String> illegalWords = Stream.of("murder", "crime").collect(Collectors.toList());
        illegalWords.forEach( word -> {
            if (todo.getName().toLowerCase().contains(word)
                    || (todo.getDescription() != null && todo.getDescription().toLowerCase().contains(word))){
                throw new TodoUnacceptableException(String.format("Word '%s' is forbidden", word));
            }
        });
    }

    private void validateBusinessRulesForUpdate(Todo exitingTodo, Todo update){
        if (!exitingTodo.getId().equals(update.getId())){
            throw new CannotUpdateIdInTodoException();
        }
        //There could be many other validations
    }
}
