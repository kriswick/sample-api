package com.kriswick.todoapi.service;

import com.kriswick.todoapi.model.Todo;
import com.kriswick.todoapi.model.TodoCreate;
import com.kriswick.todoapi.model.TodoFilter;

import java.util.List;
import java.util.UUID;

public interface TodoService {

    List<Todo> list(TodoFilter todoFilter);

    Todo create(TodoCreate todo);

    Todo replace(UUID id, Todo todo);

    Todo get(UUID id);
}
