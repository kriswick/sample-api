package com.kriswick.todoapi.repository;

import com.kriswick.todoapi.model.Todo;
import com.kriswick.todoapi.model.TodoStatus;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class TodoRepositoryInMemoryImpl implements TodoRepository {

    private final Map<UUID, Todo> store;

    public TodoRepositoryInMemoryImpl() {
        this.store = Collections.synchronizedMap(new LinkedHashMap<UUID, Todo>());
    }

    @Override
    public Todo save(Todo todo) {
        store.put(todo.getId(), todo);
        return todo;
    }

    @Override
    public Optional<Todo> findOne(UUID id) {
        return Optional.ofNullable(store.get(id));
    }

    @Override
    public List<Todo> findAll() {
        return new ArrayList<Todo>(store.values());
    }

    @Override
    public List<Todo> findAllByStatus(TodoStatus status) {
        return store.values()
                .stream()
                .filter(todo -> todo.getStatus() == status)
                .collect(Collectors.toList());
    }
}
