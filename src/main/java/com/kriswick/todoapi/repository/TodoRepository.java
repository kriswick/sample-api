package com.kriswick.todoapi.repository;

import com.kriswick.todoapi.model.Todo;
import com.kriswick.todoapi.model.TodoStatus;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TodoRepository {

    public Todo save(Todo todo);

    Optional<Todo> findOne(UUID id);

    public List<Todo> findAll();

    public List<Todo> findAllByStatus(TodoStatus status);

}
