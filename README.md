### todo-api
This is a sample api implemented in Java8/Springboot. It is a REST Api backend for a simple todo application,
which allows you to create, update and retrieve hypothetical todo task resources.

## Running the applications

### todo-api

**Prerequisites** Java (>v8)

todo-api can be started either using the included jar as follows (from the folder containing this file)
```
java -jar ./todo-api/build/lib/todo-api-0.0.1.jar
```
Or, it can be started using included gradle-wrapper.
```
cd todo-api
./gradlew bootRun
```
This will start the todo-api on port 8080.

